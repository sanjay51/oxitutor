import { UIInteractionService } from './shared/ui-interaction.service';
import { InstructionsEditorWidgetComponent } from './editor/instructions-editor-widget/instructions-editor-widget.component';
import { EditorHelperService } from './editor/editor-helper.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard.component';
import { CourseComponent } from './course.component';
import { LoginComponent } from './login/login.component';
import { EditorComponent } from './editor/editor.component';
import { SectionEditComponent } from './editor/section-edit/section-edit.component';
import { CourseMetadataEditComponent } from './editor/course-metadata-edit/course-metadata-edit.component';
import { ChapterMetadataEditComponent } from './editor/chapter-metadata-edit/chapter-metadata-edit.component';
import { StorageService } from './shared/storage.service';
import { Utils } from './shared/utils.service';
import { LogService } from './shared/log.service';
import { ValidatorService } from './shared/validator.service';
import { ImageHelperService } from './shared/image-helper.service';
import { MarkupHelperService } from './shared/markup-helper.service';

import { CoursesService } from './shared/courses.service';
import { RuleEvaluatorService } from './rule-evaluator.service';
import { AuthenticationService } from './shared/authentication.service';
import { LoginGuard } from './login/login-guard.service';
import { SignupGuard } from './signup/signup-guard.service';
import { CoursePreviewComponent } from './course-preview/course-preview.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
import { NewCourseComponent } from './new-course/new-course.component';
import { FooterComponent } from './footer/footer.component';
import { EditorOptionsComponent } from './editor/editor-options/editor-options.component';
import { FeedbackComponent } from './feedback/feedback.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        BrowserAnimationsModule
    ],

    declarations: [
        AppComponent,
        HeaderComponent,
        DashboardComponent,
        CourseComponent,
        LoginComponent,
        EditorComponent,
        SectionEditComponent,
        CourseMetadataEditComponent,
        ChapterMetadataEditComponent,
        CoursePreviewComponent,
        SignupComponent,
        ProfileComponent,
        NewCourseComponent,
        FooterComponent,
        EditorOptionsComponent,
        InstructionsEditorWidgetComponent,
        FeedbackComponent
    ],
    bootstrap: [AppComponent],
    providers: [AuthenticationService, CoursesService, RuleEvaluatorService,
        StorageService, LoginGuard,
        SignupGuard, ValidatorService, Utils, LogService,
        ImageHelperService, MarkupHelperService, EditorHelperService,
        UIInteractionService]
})

export class AppModule { }
