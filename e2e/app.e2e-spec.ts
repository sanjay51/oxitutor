import { OxiTutorPage } from './app.po';

describe('oxi-tutor App', () => {
  let page: OxiTutorPage;

  beforeEach(() => {
    page = new OxiTutorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
